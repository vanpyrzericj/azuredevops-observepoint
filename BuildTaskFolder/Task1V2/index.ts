import tl = require("azure-pipelines-task-lib/task");
const axios = require("axios").default;

function splitAndConvert(input: string) {
  if (input != "") {
    return input.split(",").map(function(item) {
      return parseInt(item.trim());
    });
  } else {
    return [];
  }
}

async function run() {
  try {
    //Setup ObservePoint API Service Endpoint
    const opServiceEndpoint = tl.getInput("observepointservice", true) || "";
    const opServiceRunnerEndpoint = "/v2/cards/run";

    if (opServiceEndpoint == "")
      tl.setResult(
        tl.TaskResult.Failed,
        "ObservePoint Service Endpoint Is Required."
      );

    const opEndpointUrl = tl.getEndpointUrl(opServiceEndpoint, false);
    const opEndpointAuthorizationParameter = tl.getEndpointAuthorizationParameter(
      opServiceEndpoint,
      "apitoken",
      false
    );

    //Get IDs Requested for Runner
    const auditIds: string | undefined = tl.getInput("auditids", false) || "";
    const webjourneyIds: string | undefined =
      tl.getInput("webjourneyids", false) || "";
    const appjourneyIds: string | undefined =
      tl.getInput("appjourneyids", false) || "";

    console.log("ObservePoint Runner");
    console.log(
      "=============================================================================="
    );

    //Validate Input from DevOps
    if (auditIds == "bad" || webjourneyIds == "bad" || appjourneyIds == "bad") {
      tl.setResult(tl.TaskResult.Failed, "Bad input was given");
      return;
    }

    //Validate Inputs for Skipping
    if (auditIds == "" && webjourneyIds == "" && appjourneyIds == "") {
      tl.setResult(
        tl.TaskResult.Skipped,
        "You did not specify any audits or journeys to run. Task skipped."
      );
      return;
    }

    //Validate Input

    console.log("Requesting the following runs:");
    console.log("Audits: ", auditIds != "" ? auditIds : "None");
    console.log("Web Journeys: ", webjourneyIds != "" ? webjourneyIds : "None");
    console.log("App Journeys: ", appjourneyIds != "" ? appjourneyIds : "None");

    let payload = {
      auditIds: splitAndConvert(auditIds),
      webJourneyIds: splitAndConvert(webjourneyIds),
      appJourneyIds: splitAndConvert(appjourneyIds)
    };

    console.log(
      "=============================================================================="
    );

    console.log("Connecting to Service Endpoint.");
    console.log(
      "=============================================================================="
    );
    console.log("Posting: ", `${opEndpointUrl}${opServiceRunnerEndpoint}`);
    console.log(
      "Authorization Header: ",
      `api_key ${opEndpointAuthorizationParameter}`
    );
    console.log("Payload: ", payload);

    axios({
      baseURL: opEndpointUrl,
      url: opServiceRunnerEndpoint,
      method: "post",
      headers: { Authorization: `api_key ${opEndpointAuthorizationParameter}` },
      data: payload
    })
      .then(function(response) {
        console.log(
          "=============================================================================="
        );
        console.log("Success response ", response.data);
        //Determine if any runs failed to start
        if (
          response.data.failedToStartAudits.length > 0 ||
          response.data.failedToStartWebJourneys.length > 0 ||
          response.data.failedToStartAppJourneys.length > 0
        ) {
          console.log("Some Runners Failed to Start: ", response.data);
          tl.setResult(
            tl.TaskResult.SucceededWithIssues,
            "Some Runners Failed to Start. See Log Output for Details."
          );
        }

        //Otherwise, just return successful.
        console.log("Successfully Started All Runners.");
      })
      .catch(function(response) {
        console.log(
          "=============================================================================="
        );
        console.log("Failure response ", response.data);
        //Handle Error(s) from ObservePoint
        console.log(
          "ObservePoint Returned One or More Errors: ",
          response.data.message
        );
        tl.setResult(
          tl.TaskResult.Failed,
          "ObservePoint Returned One or More Errors. See Log Output for details."
        );
      });
    console.log(
      "=============================================================================="
    );
  } catch (err) {
    tl.setResult(tl.TaskResult.Failed, err.message);
  }
}

run();
