# Never Break Analytics Again

The ObservePoint runner build task (unofficial) leverages ObservePoint's API to trigger audit runs without having to manually execute cURL commands. This simple interface allows users with the correct privilages to add one or more audits to run as part of your CI/CD process, ensuring that any rule failures set in ObservePoint are caught before deploying to production.

**Requires an active contract for ObservePoint's WebAssurance software. Author provides no such contract, or warranty for the extension. The use of this extension is subject to the Terms of Service of your ObservePoint contract.**

## How To Use

After installing this extension from the marketplace to your DevOps organization:

1.  Navigate to **Project Settings** > **Service Connections**
2.  Add a **New Service Connection**, selecting _ObservePoint_ from the connection type.
    ![](images/ServiceEndpoint-Screen1.png)

3.  Enter your API Key (found in ObservePoint under your profile settings). 99% of cases you will not need to modify the default server URL.

    ![](images/ServiceEndpoint-Screen2.png)

4.  Create or modify an existing **Pipeline** (build).
5.  Add a new **Build** task and search for the _Trigger ObservePoint Audit/Journey Runs_ task.
    ![](images/AddTasks.png)

6.  Select your service endpoint you created in step #2 and then add one or more audits or journeys, comma separated, by their ID. You can find the ID for an audit or journey by looking at the URL from within ObservePoint.
    ![](images/TaskProperties.png)

7.  When the task runs, it will POST to ObservePoint's API, triggering the audit runs. The task will return _Failed_ if ObservePoint returns one or more errors (typically permissions-related). If some audits fails to start, the task will return _Successful with Issues_. Check the task's logging output for details on the response from ObservePoint's endpoint.
    ![](images/RunnerOutput.png)

## Limitations

The ObservePoint API does not return the run IDs for successful runs, so there is no way to automatically capture that information. It is recommended that you set email, slack or Jira notifications in ObservePoint itself, or leverage their webhook functionality, to be notified of failed audits (breaking builds).

## Contributions

Improvements, feature requests and additions are always welcome, and can be facilitated through the [GitLab public repository](https://gitlab.com/vanpyrzericj/azuredevops-observepoint) for this extension.
